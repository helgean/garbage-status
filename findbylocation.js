import repository from './repository.js';

class GarbageFindLocation extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
  }

  getCoordinates() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, {enableHighAccuracy: true, timeout: 10000});
    });
  }

  async getContainers(latitude, longitude) {
    const data = await repository.findNearest(58.9874024, 5.6893904);
    /*
      // Group by address
      data
      .reduce((acc, curr) => {
        (acc[curr.address] || []).push(curr);
        return acc;
      }, {})
    */
    return data.map(d => { return { addr: d.address, dist: d.distMeters, type: d.fraction  }; }).slice(0, 12);
  }

  async getTemplate() {
    var containers = await this.getContainers(this.currentPos.latitude, this.currentPos.longitude);

    return `
      <garbage-header>
        I nærheten
      </garbage-header>
      ${JSON.stringify(this.currentPos)}
      <div>
      ${JSON.stringify(containers)}
      </div>
    `;
  }

  async connectedCallback() {
    const currLoc = await this.getCoordinates();
    this.currentPos = { latitude: currLoc.coords.latitude, longitude: currLoc.coords.longitude };
    await this.render();
  }

  async render() {
    this.innerHTML = await this.getTemplate();
  }
}

customElements.define('garbage-find-location', GarbageFindLocation);