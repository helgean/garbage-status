class GarbageHeader extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();

    this.content = this.innerHTML;
  }

  getTemplate() {
    return `
      <header class="w3-top">
        <div class="w3-row w3-padding w3-theme-d2 w3-xlarge header-main-content">
          <div class="w3-quarter">
            <div class="w3-bar">
              <a href="#" class="w3-bar-item w3-button"><i class="material-icons md-24">menu</i></a>
            </div>
          </div>
          ${this.content}
        </div>
      </header>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = this.getTemplate();
  }
}

customElements.define('garbage-header', GarbageHeader);