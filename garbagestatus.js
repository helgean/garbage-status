class GarbageStatus extends HTMLElement {
    constructor() {
      // Always call super first in constructor
      super();
    }

    getTemplate() {
      return `
          <div class="container-cube">
            <div class="cube ${this.garbagetype.toLowerCase()}">
              <div class="side front">
                <div class="side level" style="height: ${this.usedspace}%"></div>
                <span>${this.garbagetype}<br>${this.usedspace}%</span>
              </div>
              <div class="side back">&nbsp;</div>
              <div class="side right">&nbsp;</div>
              <div class="side left">&nbsp;</div>
              <div class="side top">&nbsp;</div>
              <div class="side bottom">&nbsp;</div>
              <div class="side top-level" style="top: ${this.usedspace}%">&nbsp;</div>
            </div>
          </div>
      `;
    }

    connectedCallback() {
      this.render();
    }

    render() {
      this.innerHTML = this.getTemplate();
    }

    static get observedAttributes() {
      return ['usedspace'];
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
      if (newValue !== oldValue) {
        this[attrName] = newValue;
        this.render();
      }
    }

    get usedspace() {
      return this.getAttribute('usedspace');
    }

    set usedspace(value) {
      this.setAttribute('usedspace', value ? value : 0);
    }

    get garbagetype() {
      return this.getAttribute('garbagetype');
    }

    set garbagetype(value) {
      this.setAttribute('garbagetype', value ? value : '');
    }
}

customElements.define('garbage-status', GarbageStatus);