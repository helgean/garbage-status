class GarbageFind extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
  }

  getTemplate() {
    return `
    <garbage-header>
      Finn nedgravde containere
    </garbage-header>
    <div class="w3-container">
      <a href="#findbylocation" class="large-nav-button w3-button w3-theme w3-round w3-ripple">
        <i class="material-icons">location_searching</i>
      </a>
      <a href="#findbyaddress" class="large-nav-button w3-button w3-theme w3-round w3-ripple">
        <i class="material-icons">search</i>
      </a>
    </div>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = this.getTemplate();
  }
}

customElements.define('garbage-find', GarbageFind);