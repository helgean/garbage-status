class GarbageGroup extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();

    this.content = this.innerHTML;
  }

  getTemplate() {
    return `
      <div class="w3-cell w3-container">
        <h3>${this.title}</h3>
        ${this.content}
      </div>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = this.getTemplate();
  }

  get title() {
    return this.getAttribute('title');
  }

  set title(value) {
    this.setAttribute('title', value ? value : '');
  }
}

customElements.define('garbage-group', GarbageGroup);