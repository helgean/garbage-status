  import repository from './repository.js';

  class GarbageContainers extends HTMLElement {
    constructor() {
      // Always call super first in constructor
      super();
    }

    async getContainers() {
      const data = await repository.findByAddress("gjerdehagen");

      const containerLocation = data
        .reduce((groups, container) => {
          if (!groups[container.address]) groups[container.address] = {};
          groups[container.address][container.fraction.toLowerCase()] = container;
          return groups;
        }, {});

      const containerGroups = Object.keys(containerLocation).map(key => {
        return { address: key, containers: containerLocation[key] };
      });

      let containersHtml = containerGroups.reduce((html, containerGroup) => {
        // TODO: Create element with all three container types with level and info
        html += `<garbage-group title="${containerGroup.address}">`;

        let container = containerGroup.containers.bio;
        html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;
        container = containerGroup.containers.papir;
        html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;
        container = containerGroup.containers.restavfall;
        html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;
        /*
        containerGroup.containers.forEach(container => {
          let pct = Math.round((container.counter * 100) / container.fillheight);
          html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;
        });*/

        html += `</garbage-group>`;

        return html;
      }, '');

      return containersHtml;
    }

    async getTemplate() {
      let containers = await this.getContainers();

      return `
        <div class="w3-container w3-theme">
          <h1>Søppelkontainer status</h1>
          <br>
        </div>
        <div class="container-list w3-container w3-theme">
          ${containers}
        </div>
        <button id="get-btn">Get</button>
        `;
    }

    async connectedCallback() {
      await this.render();
      //this.shadowRoot.getElementById("get-btn").addEventListener('click', ev => this.fetchCsvData());
      this.getElementById("get-btn").addEventListener('click', ev => this.fetchCsvData());
    }

    async render() {
      //this.shadowRoot.innerHTML = await this.getTemplate();
      this.innerHTML = await this.getTemplate();
    }

    async renderContainers() {
      //this.shadowRoot.querySelector('.container-list').innerHTML = await this.getContainers();
      this.querySelector('.container-list').innerHTML = await this.getContainers();
    }

    async onLoad() {
      await this.renderContainers();
    }

    async fetchCsvData() {
      const url = "https://opencom.no/dataset/8c10f9c2-d0f1-4ef6-91ac-9094acfe55f7/resource/1207f3c9-d8a9-44f8-b320-154493b9157d/download/ren_nedgravde-containere.csv";
      let response = await fetch(url);
      let result = await response.text();
      this.data = await new Promise((resolve, reject) => {
        let data = [];
        Papa.parse(result, {
          delimiter: "", // auto-detect
          newline: "",	// auto-detect
          quoteChar: "'",
          header: true,
          transformHeader: header => { return header.toLowerCase() },
          dynamicTyping: false,
          preview: 0,
          step: (results, parser) => {
            data.push(results.data);
          },
          complete: () => {
            resolve(data);
          },
          error: error => {
            reject(error);
          },
          skipEmptyLines: true,
          transform: (value, field) => {
            const schema = repository.containerSchema;
            const type = schema[field];

            if (type === Date)
              return moment(value, 'DD.MM.YYYY').toDate();
            if (type === Number)
              return parseFloat(value);
            if (type === Boolean)
              return value === true || (typeof value == 'string' && value.toLowerCase() == 'true');
            return value;
          }
        });
      });

      await repository.replaceAll(this.data);

      await this.onLoad();
    }

    get data() {
      return this._data ? this._data : [];
    }

    set data(value) {
      this._data = value;
    }
  }

  customElements.define('garbage-containers', GarbageContainers);

