class GarbageFindAddress extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
  }

  getTemplate() {
    return `
    <garbage-header>

      <div class="w3-half">
        <input type="text" class="w3-amber w3-border-0 w3-padding" style="width:100%">
      </div>

      <div class="w3-quarter">
        <div class="w3-bar w3-xlarge">
          <a href="#" class="w3-bar-item w3-button w3-left"><i class="material-icons md-36">search</i></a>
        </div>

      </div>
    </garbage-header>

    `;
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = this.getTemplate();
  }
}

customElements.define('garbage-find-address', GarbageFindAddress);