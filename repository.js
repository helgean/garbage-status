class Repository {
  constructor() {
    this.db = new Dexie("garbage");
    this.db.version(1).stores({
        container: '&containernumber, address, latitude, longitude, fraction',
        info: '++id, updated, latitude, longitude, *addresses'
    });

    this.containerSchema = {
        containernumber: String,
        address: String,
        latitude: Number,
        longitude: Number,
        fraction: String,
        counter: Number,
        fillheight: Number,
        date: Date,
        temperature: Number
    };

    this.infoSchema = {
      id: Number,
      updated: Date,
      latitude: Number,
      longitude: Number,
      addresses: [String]
  };

    this.db.container.defineClass(this.containerSchema);
    this.db.info.defineClass(this.infoSchema);
  }

  async updateInfo(info) {
    await this.db.info.put(info, 1);
  }

  async getInfo() {
    return await this.db.info.get(1);
  }

  async findByAddress(startsWithAddress) {
    return await this.db.container
      .where("address")
      .startsWithAnyOfIgnoreCase([startsWithAddress])
      .toArray();
  }

  async findByAddresses(addresses) {
    return await this.db.container
      .where("address")
      .startsWithAnyOfIgnoreCase(addresses)
      .toArray();
  }

  async findNearest(latitude, longitude) {
    const contArr = await this.db.container.toArray();
    return contArr
      .map(c => {
        c.dist = this.distance(latitude, longitude, c.latitude, c.longitude, 'K');
        c.distMeters = Math.round(this.distance(latitude, longitude, c.latitude, c.longitude, 'K') * 1000);
        return c;
      })
      .filter(c => {
        return c.dist < 1;
      })
      .sort((a, b) => a.dist - b.dist);
  }


  async replaceAll(data) {
    const lastKey = await this.db.transaction('rw', this.db.container, async () => {
      await this.db.container.clear();
      return await this.db.container.bulkAdd(data);
    });
    return lastKey;
  }

  containerSchema() {
    return this.containerSchema;
  }

  distance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == lat2 && lon1 == lon2)
      return 0;

    const radlat1 = Math.PI * lat1/180;
    const radlat2 = Math.PI * lat2/180;
    const theta = lon1-lon2;
    const radtheta = Math.PI * theta/180;

    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1)
      dist = 1;

    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit=="K") { dist = dist * 1.609344 } //Kilometer
    if (unit=="N") { dist = dist * 0.8684 } //Nautical miles
    return dist;
  }
}

export default new Repository();