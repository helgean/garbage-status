import Router from './router.js';
import Syncronize from './syncronize.js';

class GarbageHome extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();

    this.router = new Router({
      '': 'home',
      'find': 'find',
      'findbylocation': 'findbylocation',
      'findbyaddress': 'findbyaddress'
    }, {
      stateChange: (section, path) => {
        this.render(section);
      }
    });
  }

  getTemplate() {
    return `
      <garbage-header>
        <div class="w3-half">Mine søppelcontainere</div>
      </garbage-header>
      <garbage-my-container-list></garbage-my-container-list>
    `;
  }

  connectedCallback() {
    this.render();
  }

  render(section) {
    if (!section || section == 'home')
      this.innerHTML = this.getTemplate();
    else if (section == 'find')
      this.innerHTML = '<garbage-find></garbage-find>';
    else if (section == 'findbylocation')
      this.innerHTML = '<garbage-find-location></garbage-find-location>';
    else if (section == 'findbyaddress')
      this.innerHTML = '<garbage-find-address></garbage-find-address>';
  }
}

customElements.define('garbage-home', GarbageHome);