import repository from './repository.js';

class GarbageMyContainerList extends HTMLElement {
  constructor() {
    // Always call super first in constructor
    super();
  }

  async getContainers() {
      const data = await repository.findByAddress("gjerdehagen");

      const containerLocation = data
        .reduce((groups, container) => {
          if (!groups[container.address]) groups[container.address] = {};
          groups[container.address][container.fraction.toLowerCase()] = container;
          return groups;
        }, {});

      const containerGroups = Object.keys(containerLocation).map(key => {
        return { address: key, containers: containerLocation[key] };
      });

      let containersHtml = containerGroups.reduce((html, containerGroup) => {
        // TODO: Create element with all three container types with level and info
        html += `<garbage-group title="${containerGroup.address}">`;

        let container = containerGroup.containers.bio;
        let pct = Math.round((container.counter * 100) / container.fillheight);
        html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;
        container = containerGroup.containers.papir;
        pct = Math.round((container.counter * 100) / container.fillheight);
        html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;
        container = containerGroup.containers.restavfall;
        pct = Math.round((container.counter * 100) / container.fillheight);
        html += `<garbage-status usedspace="${pct}" garbagetype="${container.fraction}"></garbage-status>`;

        html += `</garbage-group>`;

        return html;
      }, '');

    return containersHtml;
  }

  async getTemplate() {
    let containers = await this.getContainers();

    return `
      <div class="w3-container w3-theme">
        <h1></h1>
        <br>
      </div>
      <div class="container-list w3-container w3-theme">
        ${containers}
      </div>
      <div class="w3-container center-content">
        <a href="#find" class="large-nav-button w3-button w3-theme w3-round w3-ripple"><i class="material-icons">add</i></a>
      </div>
      `;
  }

  async connectedCallback() {
    await this.render();
  }

  async render() {
    //this.shadowRoot.innerHTML = await this.getTemplate();
    this.innerHTML = await this.getTemplate();
  }

  async renderContainers() {
    this.querySelector('.container-list').innerHTML = await this.getContainers();
  }

  async onLoad() {
    await this.renderContainers();
  }

  get data() {
    return this._data ? this._data : [];
  }

  set data(value) {
    this._data = value;
  }
}

customElements.define('garbage-my-container-list', GarbageMyContainerList);

