class Router {
    constructor(routes, events) {
        this.routes = routes;

        window.onpopstate = () => {
            this.setState();
        };

        this.setState();

        if (events) {
            this.stateChange = events.stateChange;
            this.beforeChange = events.beforeChange;
        }
    }

    setState() {
        let newPath = window.location.hash.slice(1);
        let newSection = this.routes[newPath];

        this.onBeforeStateChange(newSection, newPath);

        this.path = newPath;
        this.section = newSection;
        this.onStateChange();
    }

    navigate(path) {
        let newPath = path;
        let newSection = this.routes[newPath];

        this.onBeforeStateChange(newSection, newPath);

        this.section = newSection;
        this.path = newPath;

        window.history.pushState(
            {},
            section,
            `${window.location.origin}#${newPath}`
        );
        this.onStateChange();
    }

    onStateChange() {
        if (this.stateChange)
            this.stateChange(this.section, this.path);
    }

    onBeforeStateChange(newSection, newPath) {
        if (this.beforeChange)
            this.beforeChange(newSection, this.section, newPath, this.path);
    }
}

export default Router;