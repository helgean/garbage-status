import repository from './repository.js';

class Syncronize {
  static async fetchCsvData() {
    const url = "https://opencom.no/dataset/8c10f9c2-d0f1-4ef6-91ac-9094acfe55f7/resource/1207f3c9-d8a9-44f8-b320-154493b9157d/download/ren_nedgravde-containere.csv";
    let response = await fetch(url);
    let result = await response.text();
    this.data = await new Promise((resolve, reject) => {
      let data = [];
      Papa.parse(result, {
        delimiter: "", // auto-detect
        newline: "",	// auto-detect
        quoteChar: "'",
        header: true,
        transformHeader: header => { return header.toLowerCase() },
        dynamicTyping: false,
        preview: 0,
        step: (results, parser) => {
          data.push(results.data);
        },
        complete: () => {
          resolve(data);
        },
        error: error => {
          reject(error);
        },
        skipEmptyLines: true,
        transform: (value, field) => {
          const schema = repository.containerSchema;
          const type = schema[field];

          if (type === Date)
            return moment(value, 'DD.MM.YYYY').toDate();
          if (type === Number)
            return parseFloat(value);
          if (type === Boolean)
            return value === true || (typeof value == 'string' && value.toLowerCase() == 'true');
          return value;
        }
      });
    });

    await repository.replaceAll(this.data);
  }
}

export default Syncronize;